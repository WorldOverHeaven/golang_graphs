package dto

type Result struct {
	UserID        int64
	TaskVariantID int64
	Score         int
}

// Score:
// 1 - если правильно
// 0 - если неправильно
