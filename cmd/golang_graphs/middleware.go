package main

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"golang_graphs/internal/handler/yandex_auth"
	"golang_graphs/internal/model"
	"golang_graphs/pkg/auth"
	"log"
	"net/http"
	"strings"
	"time"
)

func Logger(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		start := time.Now()

		if err := next(c); err != nil {
			c.Error(err)
		}

		log.Printf(
			"%s %s",
			c.Path(),
			time.Since(start),
		)

		return nil
	}
}

func CreateJwtMiddlewareWithService(service auth.IService) func(next echo.HandlerFunc) echo.HandlerFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			if c.Path() == "/auth" || c.Path() == "/login" || c.Path() == "/yandex" || true {
				if err := next(c); err != nil {
					c.Error(err)
				}
				return nil
			}

			token := strings.TrimPrefix(c.Request().Header.Get("Authorization"), "Bearer ")

			userId, err := service.AuthUser(token)
			if err != nil {
				return err
			}

			c.Set("userId", userId)

			fmt.Println("token", token, "id", userId)

			if err := next(c); err != nil {
				c.Error(err)
			}

			return nil
		}
	}
}

func YandexAuthMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if c.Path() == "/login" || c.Path() == "/yandex" {
			if err := next(c); err != nil {
				c.Error(err)
			}
			return nil
		}

		cookies := c.Cookies()
		fmt.Println("cookies", cookies)
		token := ""

		for _, cookie := range cookies {
			if cookie.Name == "Access-token" {
				token = cookie.Value
				break
			}
		}

		if token == "" {
			return c.JSON(http.StatusUnauthorized, model.BadRequestResponse{})
		}

		_, err := yandex_auth.ValidToken(token)
		if err != nil {
			return c.JSON(http.StatusUnauthorized, model.BadRequestResponse{})
		}

		if err := next(c); err != nil {
			c.Error(err)
		}

		return nil
	}
}
