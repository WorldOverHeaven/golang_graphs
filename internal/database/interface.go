package database

import "context"

type Database interface {
	Init(ctx context.Context) error
	CreateUser(ctx context.Context, email, password, login string) (int64, error)
	AuthUser(ctx context.Context, email string) (int64, string, error)
}
