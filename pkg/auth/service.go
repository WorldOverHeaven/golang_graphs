package auth

import (
	"fmt"
	"github.com/golang-jwt/jwt"
	"strconv"
)

type IService interface {
	CreateToken(userID int64) (string, error)
	AuthUser(tokenString string) (int64, error)
}

type Service struct {
	secret []byte
}

func NewService(secret string) IService {
	return &Service{secret: []byte(secret)}
}

func (s *Service) CreateToken(userID int64) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user": strconv.FormatInt(userID, 10),
	})

	return token.SignedString(s.secret)
}

func (s *Service) AuthUser(tokenString string) (int64, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return s.secret, nil
	})
	if err != nil {
		return 0, err
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		str, ok := claims["user"].(string)
		if !ok {
			return 0, fmt.Errorf("claim parsing failed")
		}
		userID, err := strconv.ParseInt(str, 10, 64)
		if err != nil {
			return 0, fmt.Errorf("claim parsing failed")
		}

		return userID, nil
	}

	return 0, fmt.Errorf("claim missing")
}
