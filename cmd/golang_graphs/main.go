package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"github.com/heetch/confita"
	"github.com/heetch/confita/backend/env"
	"github.com/heetch/confita/backend/file"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/swaggo/echo-swagger"
	"golang_graphs/internal/config"
	"golang_graphs/internal/controller/controller_task"
	"golang_graphs/internal/controller/controller_user"
	"golang_graphs/internal/database"
	"golang_graphs/internal/handler"
	"golang_graphs/internal/handler/files"
	"golang_graphs/internal/handler/task"
	"golang_graphs/internal/handler/user"
	"golang_graphs/internal/handler/yandex_auth"
	"golang_graphs/pkg/auth"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	_ "golang_graphs/docs"
)

var rootPath string

func init() {
	flag.StringVar(&rootPath, "rootPath", "", "root folder")
	flag.Parse()
}

// @title           Swagger Example API
// @version         1.0
// @description     This is a sample server celler server.
// @host      localhost:8000
// @securityDefinitions.basic  BasicAuth
func main() {
	cfg, err := setupCfg()
	if err != nil {
		log.Fatalf("failed to parse config: %e\n", err)
	}

	db, err := setupDb(cfg)
	if err != nil {
		log.Fatalf("db error %e\n", err)
	}

	userCtrl := controller_user.NewController(db)

	authUser := auth.NewService("123")

	authHandler := user.NewHandler(userCtrl, authUser)

	var handlerRootPath string

	if os.Getenv("ENV") == "docker" {
		handlerRootPath = "."
	} else {
		handlerRootPath = ".."
	}

	filesHandler := files.NewHandler(handlerRootPath)

	taskCtrl := controller_task.NewController()

	taskHandler := task.NewHandler(taskCtrl)

	yandexHandler := yandex_auth.NewHandler()

	e := setupEcho(authUser)

	handler.SetupRoutes(e, authHandler, filesHandler, taskHandler, yandexHandler)

	go func() {
		if err := e.Start(":" + cfg.Port); err != nil && !errors.Is(err, http.ErrServerClosed) {
			e.Logger.Fatal("shutting down the server")
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}
}

func setupDb(cfg config.Config) (database.Database, error) {
	// create a database connection
	db, err := database.NewDatabase(cfg.Postgres)

	if err != nil {
		return nil, fmt.Errorf("failed to create database conn %e\n", err)
	}

	return db, nil
}

func setupEcho(authUser auth.IService) *echo.Echo {
	e := echo.New()

	e.GET("/swagger/*", echoSwagger.WrapHandler)

	e.Use(Logger)

	e.Use(middleware.RateLimiter(middleware.NewRateLimiterMemoryStore(10)))

	//e.Use(YandexAuthMiddleware)

	//e.Use(CreateJwtMiddlewareWithService(authUser))

	renderer := &TemplateRenderer{
		templates: template.Must(template.ParseGlob(fmt.Sprintf("%s/internal/view/graphPlayground/*.html", rootPath))),
	}
	e.Renderer = renderer

	return e
}

func setupCfg() (config.Config, error) {
	ctx := context.Background()

	var cfg config.Config

	err := confita.NewLoader(
		file.NewBackend(fmt.Sprintf("%s/deploy/default.yaml", rootPath)),
		env.NewBackend(),
	).Load(ctx, &cfg)

	if err != nil {
		return config.Config{}, err
	}

	envDocker := os.Getenv("ENV")
	if envDocker != "docker" {
		cfg.Postgres.Host = "localhost"
	}

	return cfg, nil
}

// TemplateRenderer is a custom html/template renderer for Echo framework
type TemplateRenderer struct {
	templates *template.Template
}

// Render renders a template document
func (t *TemplateRenderer) Render(w io.Writer, name string, data interface{}, c echo.Context) error {

	// Add global methods if data is a map
	if viewContext, isMap := data.(map[string]interface{}); isMap {
		viewContext["reverse"] = c.Echo().Reverse
	}

	return t.templates.ExecuteTemplate(w, name, data)
}
