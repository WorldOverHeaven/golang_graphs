package dto

type User struct {
	Id        int64
	Email     string
	Password  string
	FirstName string
	LastName  string
	Group     string
	Role      string
	Salt      string
}
