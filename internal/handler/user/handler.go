package user

import (
	"context"
	"github.com/labstack/echo/v4"
	"golang_graphs/internal/consts"
	"golang_graphs/internal/controller/controller_user"
	"golang_graphs/internal/model"
	"golang_graphs/pkg/auth"
	"log"
	"net/http"
)

// Handler struct for declaring api methods
type handler struct {
	ctrl     controller_user.Controller
	authUser auth.IService
}

// NewHandler constructor for Handler, user for code generation in wire
func NewHandler(ctrl controller_user.Controller, auth auth.IService) Handler {
	return &handler{ctrl: ctrl, authUser: auth}
}

type Handler interface {
	Index(ctx echo.Context) error
	Login(ctx echo.Context) error
	Auth(ctx echo.Context) error
	Create(ctx echo.Context) error
}

// Index e.GET("/", Index)
func (h *handler) Index(ctx echo.Context) error {
	return ctx.Render(http.StatusOK, "index.html", nil)
}

func (h *handler) Login(ctx echo.Context) error {
	return ctx.Render(http.StatusOK, "login.html", nil)
}

func (h *handler) Create(ctx echo.Context) error {
	var request model.CreateUserRequest

	if err := ctx.Bind(&request); err != nil {
		log.Println(consts.ErrorDescriptions[http.StatusBadRequest], err)
		return ctx.JSON(http.StatusBadRequest, model.BadRequestResponse{})
	}

	if err := request.Validate(); err != nil {
		log.Println(consts.ErrorDescriptions[http.StatusBadRequest], err)
		return ctx.JSON(http.StatusBadRequest, model.BadRequestResponse{})
	}

	ctxBack := context.Background()

	id, err := h.ctrl.CreateUser(ctxBack, request.Email, request.Password, request.Login)
	if err != nil {
		log.Println(consts.ErrorDescriptions[http.StatusBadRequest], err)
		return ctx.JSON(http.StatusBadRequest, model.BadRequestResponse{})
	}

	token, err := h.authUser.CreateToken(id)
	if err != nil {
		log.Println(consts.ErrorDescriptions[http.StatusBadRequest], err)
		return ctx.JSON(http.StatusBadRequest, model.BadRequestResponse{})
	}

	return ctx.JSON(http.StatusOK, model.CreateUserResponse{Token: token})
}

func (h *handler) Auth(ctx echo.Context) error {
	var request model.AuthUserRequest

	if err := ctx.Bind(&request); err != nil {
		log.Println(consts.ErrorDescriptions[http.StatusBadRequest], err)
		return ctx.JSON(http.StatusBadRequest, model.BadRequestResponse{})
	}

	if err := request.Validate(); err != nil {
		log.Println(consts.ErrorDescriptions[http.StatusBadRequest], err)
		return ctx.JSON(http.StatusBadRequest, model.BadRequestResponse{})
	}

	ctxBack := context.Background()

	id, err := h.ctrl.AuthUser(ctxBack, request.Email, request.Password)
	if err != nil {
		log.Println(consts.ErrorDescriptions[http.StatusBadRequest], err)
		return ctx.JSON(http.StatusBadRequest, model.BadRequestResponse{})
	}

	token, err := h.authUser.CreateToken(id)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, model.BadRequestResponse{})
	}

	return ctx.JSON(http.StatusOK, model.AuthUserResponse{Token: token})
}
