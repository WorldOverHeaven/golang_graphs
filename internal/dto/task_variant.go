package dto

import "golang_graphs/internal/model"

type TaskVariant struct {
	ID            int64
	Graph         model.Graph
	CorrectAnswer string
	TaskModuleID  int64
}
