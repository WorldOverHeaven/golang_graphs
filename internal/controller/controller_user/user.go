package controller_user

import (
	"context"
	"fmt"
	"golang.org/x/crypto/bcrypt"
	"golang_graphs/internal/database"
	"time"
)

type controller struct {
	db database.Database
}

func NewController(db database.Database) Controller {
	return &controller{db: db}
}

type Controller interface {
	CreateUser(ctx context.Context, email, password, login string) (int64, error)
	AuthUser(ctx context.Context, email, password string) (int64, error)
}

func (c *controller) CreateUser(ctx context.Context, email, password, login string) (int64, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	hash, err := hashPassword(password)
	if err != nil {
		return 0, err
	}

	id, err := c.db.CreateUser(ctx, email, hash, login)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (c *controller) AuthUser(ctx context.Context, email, password string) (int64, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	id, hash, err := c.db.AuthUser(ctx, email)
	if err != nil {
		return 0, err
	}

	if !passwordsMatch(password, hash) {
		return 0, fmt.Errorf("wrong password")
	}

	return id, nil
}

func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func passwordsMatch(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
