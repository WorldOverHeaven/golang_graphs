package controller_task

import (
	"context"
	"golang_graphs/internal/model"
)

type Controller interface {
	TaskComponents(ctx context.Context, graph model.Graph) (int, error)
	TaskIsEulerUndirected(ctx context.Context, graph model.Graph) (bool, error)
	TaskIsBipartition(ctx context.Context, graph model.Graph) (bool, error)
}
