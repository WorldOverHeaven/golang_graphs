package handler

import (
	"github.com/labstack/echo/v4"
	"golang_graphs/internal/handler/files"
	"golang_graphs/internal/handler/task"
	"golang_graphs/internal/handler/user"
	"golang_graphs/internal/handler/yandex_auth"
)

func SetupRoutes(e *echo.Echo, auth user.Handler, files files.Handler, task task.Handler, yandex yandex_auth.Handler) {
	e.GET("/", auth.Index)
	e.GET("login", auth.Login)
	e.POST("auth", auth.Auth)
	e.POST("create", auth.Create)

	e.POST("task_components", task.TaskComponents)
	e.POST("task_is_euler", task.TaskIsEulerUndirected)
	e.POST("task_is_bipartition", task.TaskIsBipartition)

	e.GET("css/style.css", files.GetCSS)
	e.GET("js/d3.v5.min.js", files.GetJSD3)
	e.GET("js/script.js", files.GetJS)
	e.GET("favicon.ico", files.Favicon)
	e.GET("script", files.GetScript)
	e.GET("css", files.GetCSSLogin)

	e.GET("yandex", yandex.GetUserData)
}
