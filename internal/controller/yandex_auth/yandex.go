package yandex_auth

import (
	"encoding/json"
	"golang_graphs/internal/model"
	"io"
	"net/http"
)

var yandexLogin = "https://login.yandex.ru/info"

func GetYandexLoginInfo(oauthToken string) (model.UserDataYandex, error) {
	req, err := http.NewRequest(http.MethodGet, yandexLogin, nil)
	if err != nil {
		return model.UserDataYandex{}, err
	}

	req.Header.Set("Authorization", "Bearer "+oauthToken)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return model.UserDataYandex{}, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return model.UserDataYandex{}, err
	}

	var fullUserData model.UserDataYandex
	err = json.Unmarshal(body, &fullUserData)
	if err != nil {
		return model.UserDataYandex{}, err
	}

	return fullUserData, nil
}
