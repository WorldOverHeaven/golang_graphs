package yandex_auth

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"golang_graphs/internal/controller/yandex_auth"
	"golang_graphs/internal/model"
	"log"
	"net/http"
	"time"
)

// Handler struct for declaring api methods
type handler struct {
}

// NewHandler constructor for Handler, user for code generation in wire
func NewHandler() Handler {
	return &handler{}
}

type Handler interface {
	GetUserData(ctx echo.Context) error
}

func (h *handler) GetUserData(ctx echo.Context) error {
	token := ctx.Request().Header["Access-token"]

	if len(token) == 0 {
		return ctx.JSON(http.StatusBadRequest, model.BadRequestResponse{})

	}

	fmt.Println("token", token)

	info, err := ValidToken(token[0])
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, model.BadRequestResponse{})
	}

	fmt.Println(info)

	cookie := &http.Cookie{
		Name:       "Access-token",
		Value:      token[0],
		Path:       "",
		Domain:     "",
		Expires:    time.Time{},
		RawExpires: "",
		MaxAge:     0,
		Secure:     false,
		HttpOnly:   false,
		SameSite:   0,
		Raw:        "",
		Unparsed:   nil,
	}

	ctx.SetCookie(cookie)

	return ctx.JSON(http.StatusOK, info)
}

func ValidToken(accessToken string) (model.UserDataYandex, error) {
	// Check if user is valid
	userDataYandex, err := yandex_auth.GetYandexLoginInfo(accessToken)

	if err != nil {
		log.Println("yandex did not find user")
		return model.UserDataYandex{}, fmt.Errorf("yandex did not find user %w", err)
	}

	return userDataYandex, nil
}
