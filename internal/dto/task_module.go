package dto

type TaskModule struct {
	ID          int64
	Name        string
	Description string
}
